provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppetagent" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "demo"

  tags = {
    Name = "puppetagent"
  }
  vpc_security_group_ids = ["sg-00a901a571acd30f8"] 
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("demo.pem")
    host        = self.public_ip
    timeout     = "30m" # Adjust timeout as per your requirement
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet6-release-bionic.deb",
      "sudo dpkg -i puppet6-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.34.161 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent"


    ]
  }
}

output "public_ip" {
  value = aws_instance.puppetagent.public_ip
}